#include <iostream>

class Article {
public:
    virtual ~Article() {}
    virtual std::string Operation() const = 0;
};

class Coffee : public Article {
public:
    std::string Operation() const override {
        return "Coffee";
    }

    std::string Cost() const {
        return "5 dollars";
    }
};

class Decorator : public Article {
protected:
    std::shared_ptr<Article> component_;

public:
    Decorator(std::shared_ptr<Article> article) : component_(article) {
    }

    std::string Operation() const override {
        std::cout << "Calling Operation from class Decorator\n";
        return this->component_->Operation();
    }
};

class WhipCream : public Decorator {
public:
    WhipCream(std::shared_ptr<Article> component) : Decorator(component) {
    }
    std::string Operation() const override {
        std::cout << "Calling Operation from class WhipCream\n";
        return "WhipCream " + Decorator::Operation();
    }
};

class Cinnamon : public Decorator {
public:
    Cinnamon(std::shared_ptr<Article> component) : Decorator(component) {
    }

    std::string Operation() const override {
        std::cout << "Calling Operation from class Cinnamon\n";
        return "Cinnamon " + Decorator::Operation();
    }
};

void ClientCode(std::shared_ptr<Article> component) {
    std::cout << "RESULT: " << component->Operation();
}

int main() {

    //Calling derived constructor for base object.
    //The object (simple) will not have the Cost method since it is only in the derived class
    //Nevertheless it will override the Operation method, since it is virtual in base class

    std::shared_ptr<Article> simple = std::make_shared<Coffee>();
    std::cout << "Client: I've got a simple component:\n";

    ClientCode(simple);
    std::cout << "\n\n";
 
    std::shared_ptr<Article> decoratedArticle = std::make_shared<WhipCream>(simple);
    decoratedArticle = std::make_shared<Cinnamon>(decoratedArticle);

    std::cout << "Client: Now I've got a decorated component:\n";
    ClientCode(decoratedArticle);
    std::cout << "\n";

    return 0;
}
